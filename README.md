# Berichtsvorlagen deutsch

Ein Satz von Berichtsvorlagen.
Besser an in Deutschland übliche Konventionen angepaßt als die Standard-Vorlagen.

Die Datei "einzelne Begriffe" enthält eine Sammlung von Genshi-Code, der in den Tabellen verwendet wird, mit Beschreibungen, was diese Code-Schnipsel ausgeben. 

Kommentare etc. bitte an wd@trolink.de. 
